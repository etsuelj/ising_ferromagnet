"""
Code for The Ising Ferromagnet (W.Kinzel/G.Reents, Physics by Computer)
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

__author__ = "John Lawrence Euste"

class Simulation(object):
    J = 1. # interaction constant
    h = 0
    L = 100 # lattice linear dimension
    k_B = 1.
    T = 2.269
    iterate = 10000 # number of iterations
    ic = 0 # to keep track of the number of iterations
    lattice = np.random.choice([1,-1],size=[L,L])
    
    def delta_E(self,y,x):
        '''
        Calculate change in energy after flipping a random state.
        '''
        
        pl = np.empty([self.L+2,self.L+2],dtype=int) # periodic lattice
        pl[1:self.L+1,1:self.L+1] = self.lattice
        pl[0,1:self.L+1] = self.lattice[self.L-1]
        pl[self.L+1,1:self.L+1] = self.lattice[0]
        pl[1:self.L+1,0] = self.lattice[:,self.L-1]
        pl[1:self.L+1,self.L+1] = self.lattice[:,0]
        
        Y,X = y+1,x+1
        S_j = pl[Y-1,X]+pl[Y+1,X]+pl[Y,X+1]+pl[Y,X-1]
        h_i = self.J*S_j + self.h
        return -2*self.lattice[y,x]*h_i
        
    def metropolis(self,change_E,y,x,lattice,T):
        '''
        Flip the state if conditions are satisfied based on Metropolis algorithm.
        '''
        r = np.random.random()
        if r < np.exp(change_E/(self.k_B*T)):
            lattice[y,x] = -lattice[y,x]
        #note that if change_E>=0, r is always < np.exp(change_E/(k_B*T))
        return lattice
        
    def M(self,lattice):
        '''
        Determine the magnetization.
        '''
        return np.sum(lattice)/self.L

        
    def on_key(self,event):
        '''
        To make the plot interactive.
        '''
        key = event.key
        if key == 'h':
            self.h += 1
            self.ic = 0
            pamagat = 'black:-1, white:+1 \nM=%.3f \nL=%i \nT=%.3f $J/k_B$ \nh=%.3f \nnumber of iterations = %i of %i'%(self.M(self.lattice),self.L,self.T,self.h,self.ic,self.iterate)
            self.title.set_text(pamagat)
        elif key == 'g':
            self.h -= 1
            self.ic = 0
            pamagat = 'black:-1, white:+1 \nM=%.3f \nL=%i \nT=%.3f $J/k_B$ \nh=%.3f \nnumber of iterations = %i of %i'%(self.M(self.lattice),self.L,self.T,self.h,self.ic,self.iterate)
            self.title.set_text(pamagat)
        elif key == 't':
            self.T += 1
            self.ic = 0
            pamagat = 'black:-1, white:+1 \nM=%.3f \nL=%i \nT=%.3f $J/k_B$ \nh=%.3f \nnumber of iterations = %i of %i'%(self.M(self.lattice),self.L,self.T,self.h,self.ic,self.iterate)
            self.title.set_text(pamagat)
        elif key == 'r':
            self.T /= 2
            self.ic = 0
            pamagat = 'black:-1, white:+1 \nM=%.3f \nL=%i \nT=%.3f $J/k_B$ \nh=%.3f \nnumber of iterations = %i of %i'%(self.M(self.lattice),self.L,self.T,self.h,self.ic,self.iterate)
            self.title.set_text(pamagat)
        elif key == 'i':
            self.iterate+=1
            pamagat = 'black:-1, white:+1 \nM=%.3f \nL=%i \nT=%.3f $J/k_B$ \nh=%.3f \nnumber of iterations = %i of %i'%(self.M(self.lattice),self.L,self.T,self.h,self.ic,self.iterate)
        elif key == 'u':
            self.iterate-=1
            pamagat = 'black:-1, white:+1 \nM=%.3f \nL=%i \nT=%.3f $J/k_B$ \nh=%.3f \nnumber of iterations = %i of %i'%(self.M(self.lattice),self.L,self.T,self.h,self.ic,self.iterate)
            

    def continue_loop(self):
        '''
        Create a generator.
        '''
        while self.ic < self.iterate:
            self.ic += 1
            yield self.ic 

    def update(self,continue_loop):
        '''
        Perform the algorithm for simulating Ising ferromagnet.
        '''
        y,x = np.random.randint(self.L),np.random.randint(self.L) # index of the randomly chosen site
        change_E = self.delta_E(y,x)
        self.lattice = self.metropolis(change_E,y,x,self.lattice,self.T)
        self.im.set_array(self.lattice)
        pamagat = 'black:-1, white:+1 \nM=%.3f \nL=%i \nT=%.3f $J/k_B$ \nh=%.3f \nnumber of iterations = %i of %i'%(self.M(self.lattice),self.L,self.T,self.h,self.ic,self.iterate)
        self.title.set_text(pamagat)

    def animate(self):
        '''
        Animate the Ising model.
        '''
        fig = plt.figure()        
        self.im = plt.imshow(self.lattice,cmap='gray',interpolation="nearest",animated=1)
        plt.axis('off')        
        self.title = plt.title('')
        fig.canvas.mpl_connect('key_press_event', self.on_key)
        anim = FuncAnimation(fig,self.update,self.continue_loop,interval=10,repeat=0)
        plt.tight_layout()
        print('Result involves animation. Use Python console or VIDLE instead of IPython console\
 if animation is not shown. User may increase h,T, and the number of iterations as defined by\
the on_key function')        
        plt.show(block=1) # use Python console or VIDLE

if __name__ == "__main__":
    sim = Simulation()  
    sim.animate()